{-# LANGUAGE OverloadedStrings, DeriveGeneric #-}

module Account where

import GHC.Generics (Generic)
import Network.Wai
import Network.HTTP.Types
import Data.Aeson
import Data.Text (Text)
import Data.Text.Encoding
import Data.List

data Account = 
    Account { name           :: Text
            , phone          :: Text
            , covidPositive  :: Bool } deriving (Show, Generic)

instance FromJSON Account
instance ToJSON Account

fromQuery :: Query -> Either String Account 
fromQuery query = 
    let 
        findItem key = find (\(key', _) -> key' == key) query
        correctItems = map findItem ["name", "tel", "covid19"]
        mbNameItem  = findItem "name"
        mbPhoneItem = findItem "tel"
        covid19 = not $ null $ findItem "covid19"
    in
        if  length correctItems < length query then 
            Left "Some extra fields was provided"
        else case (mbNameItem, mbPhoneItem) of 
            (Nothing, _) -> Left "Couldn't find 'name' query-item"
            (_, Nothing) -> Left "Couldn't find 'tel' query-item"
            (Just nameItem, Just phoneItem) -> case (snd nameItem, snd phoneItem) of 
                (Nothing, _) -> Left "Value for 'name' wasn't provided"
                (_, Nothing) -> Left "Value for 'tel' wasn't provided"
                (Just name, Just phone) -> Right $ Account { 
                    name=decodeUtf8 name, 
                    phone=decodeUtf8 phone, 
                    covidPositive=covid19 }
