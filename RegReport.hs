{-# LANGUAGE OverloadedStrings, DeriveGeneric #-}

module RegReport where

import GHC.Generics (Generic)
import Data.Aeson
import Data.Text
import qualified Account

data RegReport = 
    RegReport { name           :: Text
              , phone          :: Text
              , covidPositive  :: Bool 
              , success        :: Bool} deriving (Show, Generic)

instance FromJSON RegReport
instance ToJSON RegReport

-- Simply transforms Account to RegReport. Could be use of registration
-- handling in the future.
fromAccount :: Account.Account -> Bool -> RegReport
fromAccount (Account.Account { 
    Account.name = name', 
    Account.phone = phone', 
    Account.covidPositive = covidPositive' }) success = 
    RegReport { 
        name = name', phone = phone', 
        covidPositive = covidPositive', success = success }
