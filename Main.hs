{-# LANGUAGE OverloadedStrings #-}
import Text.Printf
import Data.List
import Data.Aeson
import Data.Text (Text)
import Data.ByteString as BS (ByteString)
import Data.ByteString.Lazy as LBS (ByteString, fromChunks)
import Data.ByteString.Lazy.Char8 as LBS.Char8 (pack)
import Network.Wai
import Network.HTTP.Types
import Network.Wai.Handler.Warp (run)
import Account 
import RegReport

app :: Application
app request respond = do 
    response <- case rawPathInfo request of 
        "/api/covid19"  -> registerRoute request
        "/covid19.html" -> frontpageRoute request
        _               -> do return notFound
    respond response

notFound :: Response
notFound = responseLBS
        status404
        [("Content-Type", "text/plain")]
        "404 - Not Found"

badRequest :: String -> Response
badRequest error = responseLBS 
        status500
        [("Content-Type", "text/plain")]
        $ LBS.Char8.pack error

toLBS :: IO BS.ByteString -> IO LBS.ByteString
toLBS sbsio = do 
    sbs <- sbsio
    return $ LBS.fromChunks [sbs]

registerRoute :: Request -> IO Response
registerRoute request = do 
    eaccount <- (eitherDecode <$> (toLBS $ requestBody request)) :: IO (Either String Account)
    return $ if requestMethod request /= "POST" then 
        notFound
    else
        case eaccount of 
            Left error      -> badRequest error
            Right account   -> responseLBS 
                status200 
                [("Content-Type", "application/json")]
                $ encode $ fromAccount account True

frontpageRoute :: Request -> IO Response
frontpageRoute request = do 
    return $ if requestMethod request /= "GET" then 
        notFound
    else if null $ queryString request then 
        registrationPage request
    else
        registeredPage request 

registrationPage :: Request -> Response
registrationPage request = responseLBS 
    status200
    [("Content-Type", "text/html")]
    "<html><head><title>Shabak COVID-19 Registration</title></head>\
    \<body><h1>Shabak COVID-19 Registration</h1>\
    \<form>\
      \<div>\
        \<label for='name'>Name: </label>\
        \<input type='text' name='name' id='name'>\
      \</div>\
      \<div>\
        \<label for='tel'>Tel: </label>\
        \<input type='text' name='tel' id='tel'>\
      \</div>\
      \<div>\
        \<input type='checkbox' name='covid19' id='covid19'>\
        \<label for='covid19'> Tested positive for COVID-19</label>\
      \</div>\
      \<div>\
        \<input type='submit'>\
      \</div>\
    \</form>\
    \</body></html>"

renderRegistered :: RegReport -> String 
renderRegistered report = printf "\
    \<html><head><title>Shabak COVID-19 Registration</title></head>\
    \<body><h1>Shabak COVID-19 Registration</h1>\
    \<h2>Registration successful:</h2>\
    \<ul>\
      \<li>Name: %s</li>\
      \<li>Tel: %s</li>\
      \<li>COVID-19: %s</li>\
    \</ul>\
    \</body></html>" 
    (RegReport.name report) 
    (RegReport.phone report) 
    (show $ RegReport.covidPositive report)

registeredPage :: Request -> Response
registeredPage request = case fromQuery $ queryString request of 
    Left error      -> badRequest error
    Right account   -> responseLBS
        status200
        [("Content-Type", "text/html")]
        $ LBS.Char8.pack $ renderRegistered report
        where
            report = fromAccount account True 

main :: IO ()
main = do
    run 8080 app
